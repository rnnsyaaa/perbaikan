package com.company;
public interface BuyableInterface {
    public String getName();
    public double getPricePerUnit();
    public void addResource(double add);
}