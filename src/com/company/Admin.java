package com.company;

class Admin {
    private String username;
    private String password;

    public Admin(){

    }

    // Constructor
    public Admin(String username, String password){
        setUsername(username);
        setPassword(password);
    }

    // Basic Setters

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    // Basic Getters

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    // To String return a formattet string of admin
    public String toString() {
        return String.format("Name\t\t: %s \n", getUsername());
    }

    // Is match with Name and Password
    // Method to check whether the name and password is match with the attribut from the instances
    public boolean isMatch(String name, String password)
    {
        return  this.username.equals(name) && this.password.equals(password);
    }
}

